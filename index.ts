import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseWidgetTextComponent } from './src/base-widget.component';

export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseWidgetTextComponent
  ],
  exports: [
    BaseWidgetTextComponent
  ]
})
export class TextComponentModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TextComponentModule,
      providers: []
    };
  }
}
