import { Component} from '@angular/core';

@Component({
  selector: 'app-text',
  template: `<style>
.button {
  padding: 0 6px 0 6px;
  margin: 6px 8px 6px 8px;
  max-width: 300px;
  border-radius: 3px;
  font-size: 14px;
  text-align: center;
  text-decoration:none;
  border: none;
  outline: none;
}
</style>
<div class="col-xs-8">
 <button md-raised-button *ngFor="let topikBeritas of topikBerita" color="primary" class="button">
          {{topikBeritas.description | slice:0:40 : '...'}}
  </button>
</div>`,
  styles: ['']
})
export class BaseWidgetTextComponent {
  
  public topikBerita: Array<any> = 
  [
      {
        "description": "Lorem ipsum dolor sit ametasdasdasdasdawadaweaddasdwaawdadadaawdwawea"
      },
      {
        "description": "Ut enim ad minim veniam."
      },
      {
        "description": "Lorem ipsum dolor sit amet."
      },
      {
        "description": "Ut enim ad minim veniam."
      },
      {
        "description": "Lorem ipsum dolor sit amet."
      },
      {
        "description": "Ut enim ad minim veniam"
      }
  ];
  
 constructor() {
    console.log(this.topikBerita);
  }

  ngOnInit() {
  }

}
